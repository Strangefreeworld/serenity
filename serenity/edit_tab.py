import os
from pathlib import Path
import wx
import wx.stc as stc
from parser_config import PARSER_CONFIG

EOL_TEXT = {
    stc.STC_EOL_CRLF: "Windows (CRLF)",
    stc.STC_EOL_CR: "Mac (CR)",
    stc.STC_EOL_LF: "Linux (LF)",
}


def parser_data(file_extension):
    for item in PARSER_CONFIG:
        if file_extension in item["extensions"]:
            return item
    return None


NOT_FOUND_TUPLE = (-1, -1)


class Tab(wx.Panel):
    # Initialize Tab
    def __init__(self, parent):
        # Initialize wxPanel
        wx.Panel.__init__(self, parent=parent)
        self.sizer = wx.BoxSizer(wx.VERTICAL)
        self.SetSizer(self.sizer)

        # Create text control in tab
        self.text_control = stc.StyledTextCtrl(
            self, 1, style=wx.TE_MULTILINE | wx.TE_RICH2
        )
        # Set focus to text editor canvas
        wx.CallAfter(self.text_control.SetFocus)
        # Set font size & family
        self.font = wx.Font(wx.FontInfo(12))
        self.text_control.SetFont(self.font)
        self.sizer.Add(self.text_control, -1, wx.EXPAND)

        # Set text color to white
        self.text_control.SetForegroundColour(wx.BLACK)
        # Set background color to black
        self.text_control.SetBackgroundColour(wx.WHITE)

        self._init_margin()

        # Is the tab file saved?
        self._saved = False
        # Contents from the last save point
        self._full_path = None
        self._last_save = None

    def _init_margin(self):
        self.text_control.SetViewWhiteSpace(False)
        self.text_control.SetMargins(5, 0)
        self.text_control.SetMarginType(1, stc.STC_MARGIN_NUMBER)
        self.text_control.SetMarginWidth(1, 0)

    def is_modified(self):
        return self.text_control.IsModified()

    def is_empty(self):
        return self.text_control.GetValue() == ""

    def title(self):
        return os.path.basename(self._full_path) if self._full_path else "Untitled"

    @property
    def directory(self):
        return os.path.dirname(self._full_path) if self._full_path else os.getcwd()

    @property
    def full_path(self):
        return self._full_path

    @full_path.setter
    def full_path(self, full_path):
        self._full_path = full_path

    @property
    def saved(self):
        return self._saved

    @property
    def current_line(self):
        return self.text_control.GetCurrentLine() + 1

    @current_line.setter
    def current_line(self, line):
        self.text_control.GotoLine(line)

    @property
    def total_lines(self):
        return self.text_control.GetLineCount()

    @property
    def column(self):
        return self.text_control.GetColumn(self.text_control.GetCurrentPos())

    @property
    def location(self):
        current_line = self.text_control.GetCurrentLine() + 1
        total_lines = self.text_control.GetLineCount()
        column = self.text_control.GetColumn(self.text_control.GetCurrentPos())
        return f"Ln: {current_line}/{total_lines} Col: {column}"

    @saved.setter
    def saved(self, saved):
        self._saved = saved

    @property
    def current_position(self):
        return self.text_control.GetCurrentPos()

    def toggle_line_numbers(self, status):
        if status:
            self.text_control.SetMarginWidth(1, 0)
        else:
            self.text_control.SetMarginWidth(1, 35)

    def load_file(self, filepath):
        self._full_path = filepath
        self.text_control.LoadFile(filepath)
        self._last_save = self.text_control.GetValue()
        self._saved = True
        parser = parser_data(Path(self._full_path).suffix)
        if parser is not None:
            self.format_editor(parser)

    def format_editor(self, data):
        self.text_control.SetLexer(data["lexer"])
        self.text_control.SetKeyWords(0, " ".join(data["keywords"]))
        self.text_control.StyleSetSpec(wx.stc.STC_P_DEFAULT, "fore:#000000")
        self.text_control.StyleSetSpec(wx.stc.STC_P_COMMENTLINE, "fore:#008000")
        self.text_control.StyleSetSpec(wx.stc.STC_P_COMMENTBLOCK, "fore:#008000")
        self.text_control.StyleSetSpec(wx.stc.STC_P_NUMBER, "fore:#008080")
        self.text_control.StyleSetSpec(wx.stc.STC_P_STRING, "fore:#800080")
        self.text_control.StyleSetSpec(wx.stc.STC_P_CHARACTER, "fore:#800080")
        self.text_control.StyleSetSpec(wx.stc.STC_P_WORD, "fore:#000080")
        self.text_control.StyleSetSpec(wx.stc.STC_P_TRIPLE, "fore:#800080")
        self.text_control.StyleSetSpec(wx.stc.STC_P_TRIPLEDOUBLE, "fore:#800080")
        self.text_control.StyleSetSpec(wx.stc.STC_P_CLASSNAME, "fore:#0000FF")
        self.text_control.StyleSetSpec(wx.stc.STC_P_DEFNAME, "fore:#008080")
        self.text_control.StyleSetSpec(wx.stc.STC_P_OPERATOR, "fore:#800000")
        self.text_control.StyleSetSpec(wx.stc.STC_P_IDENTIFIER, "fore:#000000")

    def reset(self):
        self._full_path = None
        self._saved = False
        self._last_save = None
        self.text_control.SetValue("")

    def save(self):
        if self.text_control != self._last_save:
            self.text_control.SaveFile(self._full_path)
            self._last_save = self.text_control.GetValue()
            self._saved = True

    def eol_string(self):
        return EOL_TEXT[self.text_control.GetEOLMode()]

    def find_text(self, find_data):
        start_pos = self.text_control.GetCurrentPos()
        max_len = len(self.text_control.GetValue())
        data_found = self.text_control.FindText(
            start_pos, max_len, find_data.GetFindString(), find_data.GetFlags()
        )
        if data_found != NOT_FOUND_TUPLE:
            self.text_control.GotoPos(data_found[0])
            self.text_control.SetSelection(data_found[0], data_found[1])
            return True
        return False
