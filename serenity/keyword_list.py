c_keywords = ['auto', 'break', 'case', 'char', 'const',
              'continue', 'default', 'do', 'double', 'else',
              'enum', 'extern', 'float', 'for', 'goto', 'if',
              'int', 'long', 'register', 'return', 'short',
              'signed', 'sizeof', 'static', 'struct', 'switch',
              'typedef', 'union', 'unsigned', 'void', 'volatile', 'while']
              
javascript_keywords = ['await', 'break', 'case', 'catch', 'class', 'const',
              'continue', 'debugger', 'default', 'delete', 'do', 'else', 'enum',
              'export', 'extends', 'false', 'finally', 'for', 'function',
              'if', 'implements', 'import', 'in', 'instanceof', 'interface',
              'let', 'new', 'null', 'package', 'private', 'protected',
              'public', 'return', 'super', 'switch', 'static', 'this', 'throw',
              'try', 'True', 'typeof', 'var', 'void', 'while', 'with', 'yield']

cpp_keywords = ['asm', 'else', 'new', 'this', 'auto', 'enum', 'operator',
                'throw', 'bool', 'explicit', 'private', 'true', 'break',
                'export', 'protected', 'try', 'case', 'extern', 'public',
                'typedef', 'catch', 'false', 'register', 'typeid', 'char', 
                'float', 'reinterpret_cast', 'typename', 'class', 'for', 
                'return', 'union', 'const', 'friend', 'short', 'unsigned',
                'const_cast', 'goto', 'signed', 'using', 'continue', 'if',
                'sizeof', 'virtual', 'default', 'inline', 'static', 'void',
                'delete', 'int', 'static_cast', 'volatile', 'do', 'long',
                'struct', 'wchar_t', 'double', 'mutable', 'switch', 'while',
                'dynamic_cast', 'namespace', 'template']