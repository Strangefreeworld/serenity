import sys
from pathlib import Path


module_path = str(Path(__file__).absolute().parents[0])
if module_path not in sys.path:
    sys.path.append(module_path)
