#!/usr/bin/env python3
# Text Editor
# Author: Carl Angoli
# Date: 2021.12.09

# pylint: disable=E1101
import os
from pathlib import Path
import logging
import wx
import wx.lib.agw.flatnotebook as fnb

import app_config
from gotoline import GoToLineDialog
from edit_tab import Tab


class Frame(wx.Frame):
    # Initialize Frame
    def __init__(self, parent=None):
        # Initialize wxFrame
        wx.Frame.__init__(self, None, wx.ID_ANY, title="Serenity", size=(800, 600))

        self.panel = wx.Panel(self)
        self.sizer = wx.BoxSizer(wx.VERTICAL)
        self.panel.SetSizer(self.sizer)
        # Create the notebook
        self.notebook = fnb.FlatNotebook(self.panel)
        self.notebook.SetFont(wx.Font(wx.FontInfo(12)))
        self.line_numbers_enabled = False
        self.timer = wx.Timer(self)
        self.timer.Start(300)
        self.find_data = wx.FindReplaceData()
        self._current_dir = str(Path.home())
        # Call the high-level setup function
        self.setup_editor()

    # High-level setup function
    def setup_editor(self):
        # Setup the default tab
        self.setup_default_tab()

        # Setup the menu bar
        self.SetupMenuBar()

        # Setup Toolbar
        self.SetupToolBar()

        # Setup Keyboard shortcuts
        self.SetupKeyboardShortcuts()

        # Create the status bar
        self._create_status_bar()

        self.Layout()

    # Function to setup default tab
    def setup_default_tab(self):
        # Create the default tab
        self.default_tab = Tab(self.notebook)
        self.notebook.AddPage(self.default_tab, "Untitled")
        self.sizer.Add(self.notebook, 1, wx.EXPAND | wx.ALL)

    # Function to setup menubar
    def SetupMenuBar(self):
        # Create the menubar
        self.menubar = wx.MenuBar()
        self.menu_FILE = wx.Menu()

        self.menu_NEW = self.menu_FILE.Append(wx.ID_NEW)
        self.menu_OPEN = self.menu_FILE.Append(wx.ID_OPEN)
        self.menu_FILE.AppendSeparator()
        self.menu_SAVE = self.menu_FILE.Append(wx.ID_SAVE)
        self.menu_SAVE_AS = self.menu_FILE.Append(wx.ID_SAVEAS)
        self.menu_FILE.AppendSeparator()
        self.menu_EXIT = self.menu_FILE.Append(wx.ID_EXIT)

        self.menubar.Append(self.menu_FILE, "&File")

        self.menu_EDIT = wx.Menu()
        self.menuedit_copy = self.menu_EDIT.Append(wx.ID_COPY)
        self.menuedit_cut = self.menu_EDIT.Append(wx.ID_CUT)
        self.menuedit_paste = self.menu_EDIT.Append(wx.ID_PASTE)
        self.menu_EDIT.AppendSeparator()
        self.menu_SELECTALL = self.menu_EDIT.Append(wx.ID_SELECTALL)
        self.menubar.Append(self.menu_EDIT, "&Edit")

        self.menu_SEARCH = wx.Menu()
        self.menusearch_find = self.menu_SEARCH.Append(wx.ID_FIND)
        self.menusearch_replace = self.menu_SEARCH.Append(wx.ID_REPLACE)
        self.menusearch_goto = self.menu_SEARCH.Append(
            wx.ID_ANY, "&Goto Line...\tCtrl+G", "Goto Line Number"
        )
        self.menubar.Append(self.menu_SEARCH, "Search")

        self.menu_VIEW = wx.Menu()
        self.menuview_lineno = self.menu_VIEW.Append(
            wx.ID_ANY,
            "Toggle &Line Numbers\tAlt+Shift+L",
            "Enable/Disable line numbers",
            wx.ITEM_CHECK,
        )
        self.menubar.Append(self.menu_VIEW, "&View")
        self.SetMenuBar(self.menubar)

        self.Bind(wx.EVT_MENU, self.on_new_tab, self.menu_NEW)
        self.Bind(wx.EVT_MENU, self.on_open, self.menu_OPEN)
        self.Bind(wx.EVT_MENU, self.on_save, self.menu_SAVE)
        self.Bind(wx.EVT_MENU, self.on_save_as, self.menu_SAVE_AS)
        self.Bind(wx.EVT_MENU, self.app_exit, id=wx.ID_EXIT)
        self.Bind(wx.EVT_MENU, self.toggle_line_numbers, self.menuview_lineno)
        self.Bind(wx.EVT_MENU, self.show_find, id=wx.ID_FIND)
        self.Bind(wx.EVT_MENU, self.show_replace, id=wx.ID_REPLACE)
        self.Bind(wx.EVT_MENU, self.goto_line, self.menusearch_goto)
        self.Bind(wx.EVT_CLOSE, self.app_exit)
        self.Bind(fnb.EVT_FLATNOTEBOOK_PAGE_CLOSING, self.close_tab)

    # Function to setup keyboard shortcuts
    def SetupKeyboardShortcuts(self):
        # Setup Keyboard shortcuts
        # This will be used eventually, I think
        pass

    # Function to setup toolbar
    def SetupToolBar(self):
        self.toolbar = self.CreateToolBar()
        self.file_NEW = self.toolbar.AddTool(
            wx.ID_ANY, "New", wx.Bitmap("./icons/file_NEW.png")
        )
        self.file_OPEN = self.toolbar.AddTool(
            wx.ID_ANY, "Open", wx.Bitmap("./icons/file_OPEN.png")
        )
        self.file_SAVE = self.toolbar.AddTool(
            wx.ID_ANY, "Save", wx.Bitmap("./icons/file_SAVE.png")
        )
        self.file_SAVE_AS = self.toolbar.AddTool(
            wx.ID_ANY, "Save As...", wx.Bitmap("./icons/file_SAVE_AS.png")
        )
        self.terminal = self.toolbar.AddTool(
            wx.ID_ANY, "Terminal", wx.Bitmap("./icons/terminal.png")
        )
        self.editor_QUIT = self.toolbar.AddTool(
            wx.ID_EXIT, "Quit", wx.Bitmap("./icons/editor_QUIT.png")
        )
        self.toolbar.Realize()

        # Bind Toolbar Buttons to event handlers
        self.Bind(wx.EVT_TOOL, self.on_new_tab, self.file_NEW)
        self.Bind(wx.EVT_TOOL, self.on_open, self.file_OPEN)
        self.Bind(wx.EVT_TOOL, self.on_save, self.file_SAVE)
        self.Bind(wx.EVT_TOOL, self.on_save_as, self.file_SAVE_AS)
        self.Bind(wx.EVT_TOOL, self.on_terminal, self.terminal)
        self.Bind(wx.EVT_TOOL, self.app_exit, self.editor_QUIT)
        self.Bind(wx.EVT_TIMER, self.timer_callback)

    def _create_status_bar(self):
        self.status_bar = self.CreateStatusBar(4)
        self.status_bar.SetStatusWidths([200, 200, 125, 200])

    @property
    def current_tab(self):
        return self.notebook.GetCurrentPage()

    def goto_line(self, _):
        with GoToLineDialog(
            self,
            "Go to Line",
            self.current_tab.total_lines,
            self.current_tab.current_line + 1,
        ) as dialog:
            dialog_result = dialog.ShowModal()
            if dialog_result == wx.ID_OK:
                self.current_tab.current_line = dialog.result

    # Function to handle new tab
    def on_new_tab(self, _):
        new_tab = Tab(self.notebook)
        new_tab.SetFocus()
        self.notebook.AddPage(new_tab, "Untitled", select=True)

    def close_find(self, event):
        event.GetDialog().Destroy()

    def find_text(self, _):
        if not self.current_tab.find_text(self.find_data):
            self.status_bar.SetStatusText("Text not found", 3)

    def show_find(self, _):
        dlg = wx.FindReplaceDialog(self, self.find_data, "Find")
        self.bind_find_events(dlg)
        dlg.Show(True)

    def show_replace(self, _):
        dlg = wx.FindReplaceDialog(
            self, self.find_data, "Find and Replace", wx.FR_REPLACEDIALOG
        )
        self.bind_find_events(dlg)
        dlg.Show(True)

    def on_open(self, _):
        if self.current_tab.is_modified():
            check_save = wx.MessageBox(
                "Text has been modified. Save changes?",
                "Text Modified",
                wx.YES_NO | wx.CANCEL | wx.ICON_QUESTION,
            )
            if check_save == wx.CANCEL:
                return

        # Create a File Dialog asking for the file to open
        with wx.FileDialog(
            self,
            "Choose a file",
            wildcard="Text files (*.txt)|*.txt|Python Files|*.py|All Files|*",
            style=wx.FD_OPEN | wx.FD_FILE_MUST_EXIST | wx.FD_SHOW_HIDDEN | wx.FD_CHANGE_DIR,
        ) as dialog:
            dialog.SetDirectory(self._current_dir)
            if dialog.ShowModal() == wx.ID_CANCEL:
                return
            file_path = dialog.GetPath()
            if (
                self.notebook.GetPageCount() == 1
                and self.notebook.GetCurrentPage().is_empty()
            ):
                self.notebook.GetCurrentPage().load_file(file_path)
                self.notebook.SetPageText(
                    self.notebook.GetSelection(), self.current_tab.title()
                )
                self.status_bar.SetStatusText(
                    self.notebook.GetCurrentPage().eol_string(), 2
                )
            else:
                new_tab = Tab(self.notebook)
                new_tab.load_file(file_path)
                self.notebook.AddPage(new_tab, new_tab.title(), select=True)
                wx.CallAfter(new_tab.SetFocus)
                self.notebook.SetPageText(self.notebook.GetSelection(), new_tab.title())
                self.status_bar.SetStatusText(
                    self.notebook.GetCurrentPage().eol_string(), 2
                )
            self._current_dir = self.notebook.GetCurrentPage().directory

    def on_save(self, _):
        # Check if save is required
        current_tab = self.current_tab
        if current_tab.title() == "Untitled":
            self._save_as()
        else:
            current_tab.save()

    def on_save_as(self, _):
        self._save_as()

    def timer_callback(self, _):
        current_tab = self.notebook.GetCurrentPage()
        self.status_bar.SetStatusText(
            "Modified" if current_tab.is_modified() else "", 1
        )
        self.status_bar.SetStatusText(current_tab.location)

    def toggle_line_numbers(self, _):
        self.notebook.GetCurrentPage().toggle_line_numbers(self.line_numbers_enabled)
        self.line_numbers_enabled = not self.line_numbers_enabled

    def _save_as(self):
        current_tab = self.current_tab
        with wx.FileDialog(
            self,
            "Save File As...",
            current_tab.directory,
            "",
            "*.*",
            wx.FD_SAVE | wx.FD_OVERWRITE_PROMPT,
        ) as dialog:
            if dialog.ShowModal() == wx.ID_OK:
                current_tab.full_path = dialog.GetPath()
                current_tab.saved = False
                current_tab.save()
                self.notebook.SetPageText(
                    self.notebook.GetSelection(), current_tab.title()
                )
                self._current_dir = current_tab.directory

    def close_tab(self, e):
        current_tab = self.current_tab
        check_save = wx.NO
        if current_tab.is_modified():
            check_save = wx.MessageBox(
                "Text has been modified. Save changes?",
                "Text Modified",
                wx.YES_NO | wx.ICON_QUESTION,
            )
            if check_save == wx.CANCEL:
                return
        if check_save == wx.YES:
            if current_tab.title() == "Untitled":
                self._save_as()
            else:
                current_tab.save()

    def bind_find_events(self, dlg):
        dlg.Bind(wx.EVT_FIND, self.find_text)
        dlg.Bind(wx.EVT_FIND_NEXT, self.find_text)
        dlg.Bind(wx.EVT_FIND_REPLACE, self.replace_text)
        dlg.Bind(wx.EVT_FIND_REPLACE_ALL, self.replace_all)
        dlg.Bind(wx.EVT_FIND_CLOSE, self.close_find)

    def replace_text(self, event):
        pass

    def replace_all(self, event):
        pass

    # Start a Terminal
    def on_terminal(self, e):
        terminal_directory = self.notebook.GetCurrentPage().directory
        os.system(f"gnome-terminal --working-directory={terminal_directory}")

    # Quit Application
    def app_exit(self, e):
        tab_count = self.notebook.GetPageCount()
        curr_tab = 0
        while curr_tab < tab_count:
            if self.current_tab.is_modified():
                # print(f"Save changes to {self.notebook.GetCurrentPage().title()}")
                check_save = wx.MessageBox(
                    f"Save changes to {self.current_tab.title()}?",
                    f"{self.current_tab.title()} changed",
                    wx.YES_NO | wx.CANCEL | wx.ICON_QUESTION,
                )
                if check_save == wx.CANCEL:
                    return
                if check_save == wx.YES:
                    self.current_tab.save()
            curr_tab += 1
            self.notebook.AdvanceSelection()
        self.Destroy()


def run_editor():
    os.chdir(str(Path(__file__).absolute().parents[0]))
    # Create a wx App
    app_config.config_dir_files(["parsers.toml"])
    logging.basicConfig(
        filename=app_config.config_file_path("editor.log"),
        level=logging.INFO,
        format="%(asctime)s %(levelname)s %(threadName)s %(name)s %(message)s",
    )
    app = wx.App(False)
    # Create the editor frame
    frame = Frame()
    # Show the frame
    frame.Show()
    # Start the mainloop
    os.chdir(os.path.expanduser("~/Documents"))
    app.MainLoop()
